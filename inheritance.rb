class Hewan
    attr_accessor :name, :kaki, :nafas
    def initialize(name,kaki,nafas)
        @name = name
        @kaki = kaki
        @nafas = nafas
    end
    def info
        "#{@name} Memiliki #{@kaki} Kaki dan Bernafas menggunakan #{@nafas}"
    end
end
