class Hewan
    def initialize(name)
        @name = name
    end
    def set_method(kaki,nafas)
        @kaki = kaki
        @nafas = nafas
    end
    def get_method
        "#{@name} Memiliki #{@kaki} Kaki dan Bernafas menggunakan #{@nafas}"
    end
end

ayam = Hewan.new("Ayam")
ayam.set_method(2,"Paru paru")
puts ayam.get_method
ayam = Hewan.new("Sapi")
ayam.set_method(4,"Paru paru")
puts ayam.get_method
ayam = Hewan.new("Lele")
ayam.set_method(0,"Insang")
puts ayam.get_method