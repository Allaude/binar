def luas_segitiga a,b
    l = (a * b)/2
    return l
end

def luas_persergi a,b
    l = a * b
    return l
end

def luas_lingkaran r
    l = (22/7)*r*r
    return l
end

puts "Menghitung luas bidang"
puts "Menu : "
puts "1. Luas Lingkaran"
puts "2. Luas Segitiga"
puts "3. Luas Persegi"
puts "Masukan Pilihan Anda 1 - 3";
pilihan = gets.chomp.to_i

if pilihan < 1
    puts "Pilihan Anda Kurang dari 1"
elsif pilihan > 3
    puts "Pilihan Anda Kurang dari 3"
elsif pilihan == 1
    puts "Silahkan Input Jari jari"
    r = gets.chomp.to_i    
    hasil = luas_lingkaran(r)
    puts "luas lingkaran #{hasil}"
elsif pilihan == 2
    puts "masukan alas"
    al = gets.chomp.to_i
    puts "masukan tinggi"
    t = gets.chomp.to_i
    hasil = luas_segitiga(al,t)
    puts "luas segitiga #{hasil}"
elsif pilihan == 3
    puts "masukan alas"
    al = gets.chomp.to_i
    puts "masukan tinggi"
    t = gets.chomp.to_i
    hasil = luas_persergi(al,t)
    puts "luas segitiga #{hasil}"
end
