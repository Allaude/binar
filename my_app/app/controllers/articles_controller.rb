class ArticlesController < ApplicationController

    def index
        @articles = Article.all
        render json:@articles
    end

    def show
        @article = Article.find(params[:id])
        render json:@article
    end

    def create 
        @article = Article.create!(article_params)
        render json:@article
    end

    def update
        @article = Article.find(params[:id])
        @result = @article.update(article_params)
        render json:@result
    end

    def destroy
        @article = Article.find(params[:id])
        @result = @article.destroy
        render json:@result
    end

    private

    def article_params
        #whitelist params
        params.permit(:title, :content, :author, :category)
    end
end
