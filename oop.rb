#first oop 
=begin
class GoodDog 
    def initialize 
        puts "This Object Was Initialized"
    end
end
sparkling = GoodDog.new
=end

#instance variable
#set variable bisa dengan 2 cara set_name=("tes") pemanggilannya set_name = "tes" dan set_name("tes") pemanggilannya set_name("tes")
class GoodDog
    def initialize(name)
        @name = name
    end
    def get_name
        @name
    end
    def set_name=(name)
        @name = name
    end
    def speak
        "Hello #{@name}"
    end
end
shibuya = GoodDog.new("Shibuya")
puts shibuya.get_name
puts shibuya.speak
puts shibuya.set_name = "Mirana"
puts shibuya.speak