=begin
#global variabel diawali dengan $
$global_var = 10

class Class1 
    def info
        $global_var = 20
        puts "Global variabel class 1 adalah #$global_var"
    end
end
class Class2 
    def info
        $global_var = 30
        puts "Global variabel class 2 adalah #$global_var"
    end
end
obj1 = Class1.new
obj1.info
obj2 = Class2.new
obj2.info
=end
#instance variable diawali dengan @
class Customer
    @@total_cust = 0
    TOTAL = 100
    def initialize(id,name,addr)
        @cus_id = id
        @cus_name = name
        @cus_addr = addr
    end
    def info
        puts "Customer ID #@cus_id"
        puts "Customer Name #@cus_name"
        puts "Customer Address #@cus_addr"
    end
    def total_customer
        @@total_cust += 1
        puts "Total Customer adalah #@@total_cust dan constanta = #{TOTAL}"
    end
end

cust1 = Customer.new(1,"Kanzaki","Pekalongan")
cust2 = Customer.new(2,"Hiromi","Batang")
cust1.total_customer()
cust2.total_customer()

